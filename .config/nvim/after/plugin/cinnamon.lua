-- cinnamon.lua

require("cinnamon").setup({
    default_keymaps = true,
    always_scroll = false,
    centered = true,
    default_delay = 7,
    hide_cursor = false,
    horizontal_scroll = true,
    max_length = -1,
    scroll_limit = 150
})
