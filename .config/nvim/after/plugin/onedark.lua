-- onedark.lua

require("onedark").setup({
    style = "dark",
    transparent = true,
    term_colors = true,
    code_style = {
        comments = "italic",
        keywords = "none",
        functions = "italic",
        strings = "none",
        variables = "none"
    }
})

require("onedark").load()
