-- orgmode.lua

require("orgmode").setup_ts_grammar()

require("nvim-treesitter.configs").setup({
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = { "org" },
    },

    ensure_installed = { "org" },
})

require("orgmode").setup({
    org_agenda_files = { "~/Sync/org/*", "~/Sync/my-orgs/**/*" },
    org_default_notes_file = "~/Sync/org/refile.org",

    org_capture_templates = {
        j = {
            description = "Journal",
            template = "\n*** %<%Y-%m-%d> %<%A>\n**** %U\n\n%?",
            target = "~/Sync/org/journal.org"
        },

        n = {
            description = "Notes",
            template = "*** %<%Y-%m-%d> %<%A> ****",
            target = "~/Sync/org/notes.org"
        }
    }
})
