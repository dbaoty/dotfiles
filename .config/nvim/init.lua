-- init.lua

-- Set map leader first
vim.g.mapleader = " "

require("dbaoty.options")    -- lua/dbaoty/options.lua
require("dbaoty.remap")    -- lua/dbaoty/remap.lua
require("dbaoty.packer")    -- lua/dbaoty/packer.lua
