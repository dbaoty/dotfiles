-- options.lua

local g = vim.g
local opt = vim.opt

opt.guicursor = ""
opt.nu = true
opt.relativenumber = true
opt.tabstop = 4
opt.softtabstop = 4
opt.shiftwidth = 4
opt.expandtab = true
opt.hlsearch = false
opt.incsearch = true
opt.colorcolumn = "80"
opt.smartindent = true
opt.wrap = false
opt.swapfile = false
opt.backup = false
opt.termguicolors = true
opt.scrolloff = 8
opt.isfname:append("@-@")
opt.cmdheight = 1
opt.updatetime = 50
opt.signcolumn = "yes:2"
opt.laststatus = 3
opt.cmdheight = 1

opt.list = true
opt.listchars = {
  eol = '¬',
  tab = '· ',
  trail = '·',
  nbsp = '⦸',
  extends = '»',
  precedes = '«',
}
